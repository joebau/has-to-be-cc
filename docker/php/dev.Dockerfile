FROM php:8.0-fpm-alpine


RUN apk --update --no-cache add autoconf g++ make && \
    pecl install -f xdebug && \
    docker-php-ext-enable xdebug && \
    docker-php-ext-install bcmath && \
    apk del --purge autoconf g++ make
