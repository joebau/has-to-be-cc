# has·to·be gmbh Coding Challenge

## Tech stack
- nginx 1.21.3
- php 8.0.x
- [Lumen 8.x](https://lumen.laravel.com/)
- Docker

## Requirements

- Docker 20.10.8 (other versions will most likely work but not tested)

or:

- PHP ^8.0
- ext-bcmath
- composer

## Development environment

### Install composer packages

#### Build docker image
```shell
$ cd docker/composer
$ docker build . -t composer
```

#### Run composer install
Go to project root and run:

```shell
$ docker run --rm --interactive --tty \
  --volume $PWD/src:/app \
  composer install
```

Depending on your host system you may need to run the container with a different user (composer runs as root inside the container)

```shell
$ docker run --rm --interactive --tty \
  --volume $PWD/src:/app \
  --user $(id -u):$(id -g) \
  composer install
```

### Prepare dev environment
Go to the application src folder
```shell
$ cd src
```

Copy env.example to .env

```shell
$ cp .env.example .env
```

Set lumen application key, do not forgot to replace RANDOM_32_CHAR_STRING:
```shell
$ sed -i "" 's/^APP_KEY=.*$/APP_KEY=RANDOM_32_CHAR_STRING/' .env
```

**for production use do not forget to change APP_DEBUG to false.**


### Run dev
Go to project root and run:

```shell
$ docker-compose up
```

open "http://localhost:8080" in your browser

rate api call:

```shell
$ curl -X POST \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -d @input_example.json http://localhost:8080/rate
```

or via postman:

[Postman Collection](api.postman_collection.json)


## Testing

### Build phpunit docker image

```shell
$ cd docker/phpunit
$ docker build . -t phpunit
```

### Run tests

Go to project root and run:

```shell
$ docker run --rm --interactive --tty \
  --volume $PWD/src:/app \
  phpunit
```

## Suggests for app improvements
- Base php docker image with all required php extensions
- Exception logging (e.g. Sentry) for production
- General logging (e.g. logging to std out (docker logs), logstash)

## Suggestions for improvements to the API design (Challenge 2)
- Adding api versioning in case the data structure of the request or response has to be changed in the future - for example as part of the uri or as a request header (depending on what is better for the api consumers ) 
- Adding an optional correlation / request id in the request body or header for tracking of logs through a request chain of multiple services
- If available adding the device id of the charging station to the request and response for logging purposes
- Use of an API specification such as JSON:API for a coherent api design and ease to use
- Providing some form of Schema of the api e.g. JSON Schema or OpenApi
- Maybe add a currency field to cdr and rate
