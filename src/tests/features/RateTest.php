<?php

class RateTest extends TestCase
{

    public function testApply() {
        $this->json('POST', '/rate',
            [
                'rate' => [
                    'energy' => 0.3,
                    'time' => 2,
                    'transaction' => 1
                ],
                'cdr' => [
                    'meterStart' => 1204307,
                    'timestampStart' => '2021-04-05T10:04:00Z',
                    'meterStop' => 1215230,
                    'timestampStop' => '2021-04-05T11:27:00Z'
                ]
            ]);

        $this->response->assertOk();
        $this->response->assertJson([
            'overall' => 7.04,
            'components' => [
                'energy' => 3.277,
                'time' => 2.767,
                'transaction' => 1
            ]
        ]);
    }

    public function testApplyDifferentTimezones() {
        $this->json('POST', '/rate',
            [
                'rate' => [
                    'energy' => 0.3,
                    'time' => 2,
                    'transaction' => 1
                ],
                'cdr' => [
                    'meterStart' => 1204307,
                    'timestampStart' => '2021-04-05T11:27:00+01:00',
                    'meterStop' => 1215230,
                    'timestampStop' => '2021-04-05T11:27:00Z'
                ]
            ]);

        $this->response->assertOk();
        $this->response->assertJson([
            'overall' => 6.28,
            'components' => [
                'energy' => 3.277,
                'time' => 2,
                'transaction' => 1
            ]
        ]);
    }

    public function testApplyMeterStopGreaterOrEqualThenStart() {
        $this->json('POST', '/rate',
            [
                'rate' => [
                    'energy' => 0.3,
                    'time' => 2,
                    'transaction' => 1
                ],
                'cdr' => [
                    'meterStart' => 1215230,
                    'timestampStart' => '2021-04-05T10:27:00Z',
                    'meterStop' => 1204307,
                    'timestampStop' => '2021-04-05T11:27:00Z'
                ]
            ])->assertResponseStatus(422);
    }


    public function testApplyStartAfterStop() {
        $this->json('POST', '/rate',
            [
                'rate' => [
                    'energy' => 0.3,
                    'time' => 2,
                    'transaction' => 1
                ],
                'cdr' => [
                    'meterStart' => 1204307,
                    'timestampStart' => '2021-04-05T12:27:00Z',
                    'meterStop' => 1215230,
                    'timestampStop' => '2021-04-05T11:27:00Z'
                ]
            ])->assertResponseStatus(422);
    }

    public function testApplyBadRequest() {
        $this->json('POST', '/rate',
            [
                'rate' => [
                    'time' => 2,
                    'transaction' => 1
                ],
                'cdr' => [
                    'meterStart' => 1204307,
                    'timestampStart' => '2021-04-05T10:04:00Z',
                    'meterStop' => 1215230,
                    'timestampStop' => '2021-04-05T11:27:00Z'
                ]
            ])->assertResponseStatus(422);
    }

}
