<?php

namespace App;

use App\Dtos\ChargeDetailRecord;
use App\Dtos\Rate;
use App\Dtos\RatingComponents;
use App\Dtos\RatingResult;

class Rating
{

    /**
     * @param Rate $rate
     * @param ChargeDetailRecord $cdr
     * @return RatingResult
     */
    public function apply(Rate $rate, ChargeDetailRecord $cdr): RatingResult {
       $components = new RatingComponents(
           $rate->energy->multiply(($cdr->meterStop - $cdr->meterStart) / 1000),
           $rate->time->multiply(($cdr->timestampStop->getTimestamp() - $cdr->timestampStart->getTimestamp()) / 60 / 60),
           $rate->transaction
       );
       $overall = $components->transaction->add( $components->energy)->add($components->time);

       return new RatingResult(
           $overall,
           $components
       );
    }
}
