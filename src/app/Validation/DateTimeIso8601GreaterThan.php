<?php

namespace App\Validation;

use DateTime;
use Illuminate\Contracts\Validation\Rule;

class DateTimeIso8601GreaterThan implements Rule
{

    protected string $otherValueKey;

    public function __construct(string $otherValueKey)
    {
        $this->otherValueKey = $otherValueKey;
    }

    /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
    public function passes($attribute, $value)
    {
        if (!$this->otherValueKey) {
            return false;
        }
        $otherValue = request()->json($this->otherValueKey);

        if (!$otherValue) {
            return false;
        }

        $dateTime = DateTime::createFromFormat(\DateTimeInterface::ATOM, $value);
        $otherDateTime = DateTime::createFromFormat(\DateTimeInterface::ATOM, $otherValue);

        return $dateTime->getTimestamp() > $otherDateTime->getTimestamp();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be greater then ' . $this->otherValueKey . '.';
    }

}
