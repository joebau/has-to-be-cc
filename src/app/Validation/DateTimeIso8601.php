<?php

namespace App\Validation;

use DateTime;
use Illuminate\Contracts\Validation\Rule;

class DateTimeIso8601 implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !!DateTime::createFromFormat(\DateTimeInterface::ATOM, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be a valid ISO 8601 datetime.';
    }

}
