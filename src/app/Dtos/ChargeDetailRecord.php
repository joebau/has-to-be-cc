<?php

namespace App\Dtos;

use DateTime;

class ChargeDetailRecord
{
    public function __construct(
        public int $meterStart,
        public DateTime $timestampStart,
        public int $meterStop,
        public DateTime $timestampStop
    ) {}
}
