<?php

namespace App\Dtos;

use Spryker\DecimalObject\Decimal;


class RatingComponents
{
    public function __construct(
        public Decimal $energy,
        public Decimal $time,
        public Decimal $transaction
    ) {}
}
