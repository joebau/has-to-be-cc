<?php

namespace App\Dtos;

use Spryker\DecimalObject\Decimal;

class RatingResult
{

    public function __construct(
        public Decimal $overall,
        public RatingComponents $components
    ){}

}
