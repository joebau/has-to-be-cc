<?php

namespace App\Http\Controllers;

use App\Dtos\ChargeDetailRecord;
use App\Dtos\Rate;
use App\Rating;
use App\Validation\DateTimeIso8601;
use App\Validation\DateTimeIso8601GreaterThan;
use DateTime;
use DateTimeInterface;
use Illuminate\Http\Request;

use Spryker\DecimalObject\Decimal;

class RateController extends Controller
{

    public function apply(Request $request)  {
        $this->validate($request, [
            'rate.energy' => 'required|numeric',
            'rate.time' => 'required|numeric',
            'rate.transaction' => 'required|numeric',

            'cdr.meterStart' => 'required|integer',
            'cdr.timestampStart' => ['required', new DateTimeIso8601()],
            'cdr.meterStop' => 'required|integer|gte:cdr.meterStart',
            'cdr.timestampStop' => ['required', new DateTimeIso8601(), new DateTimeIso8601GreaterThan('cdr.timestampStart')],
        ]);


        $rate = new Rate(
            new Decimal($request->input('rate.energy')),
            new Decimal($request->input('rate.time')),
            new Decimal($request->input('rate.transaction'))
        );


        $cdr = new ChargeDetailRecord(
            $request->input('cdr.meterStart'),
            DateTime::createFromFormat(DateTime::ATOM, $request->input('cdr.timestampStart')),
            $request->input('cdr.meterStop'),
            DateTime::createFromFormat(DateTime::ATOM, $request->input('cdr.timestampStop')),
        );



        $rating = new Rating();
        $result = $rating->apply($rate, $cdr);

        return response()->json([
            'overall' => $result->overall->round(2)->toFloat(),
            'components' => [
                'energy' => $result->components->energy->round(3)->toFloat(),
                'time' => $result->components->time->round(3)->toFloat(),
                'transaction' => $result->components->transaction->round(3)->toFloat()
            ]
        ]);
    }

}
